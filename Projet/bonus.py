import os, os.path, sys, time
os.chdir('./File')
import matplotlib.pyplot as pyplot

#Construct a histogram
def process_file_module2_1():

    try:
        infile = open('wagon.txt', 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        counts = {} # Counts each words in the file in store them in a dic as key -> value

        for w in words:
            counts[w] = counts.get(w, 0) + 1
        
        return counts

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")

def rankFreq(counts):

    freqs = list(counts.values())
    freqs.sort(reverse=True)

    rankFreq = [(rank + 1, freq) for rank, freq in enumerate(freqs)]

    for rank, freq in rankFreq:
        print(rank, freq)

    return rankFreq

def plotRank(counts, scale='log'):
    
    result = rankFreq(counts)
    rank, freq = zip(*result)

    pyplot.clf()
    pyplot.xscale(scale)
    pyplot.yscale(scale)
    pyplot.title('Zipf\' law')
    pyplot.xlabel('rank')
    pyplot.ylabel('frequency')
    pyplot.plot(rank, freq, '*')
    pyplot.show()

def main():
    
    histogram = process_file_module2_1()
    
    plotRank(histogram)
    
main()

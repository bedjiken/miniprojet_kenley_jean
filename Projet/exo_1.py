import os, time, sys, os.path
os.chdir('./File')

def process_file_module1(inputFile):

    try:
        infile = open(inputFile, 'r')
        outfile = open('result.txt', 'w')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        startTime = time.time()

        for word in words:
            outfile.write(str(word) + " ")
        endTime = time.time()

        runTime = int((endTime - startTime) * 1000)

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")
    finally:
        infile.close()
        outfile.close()

def main():

    inputFile = input("Enter the file name for reading: ").strip()

    if os.path.isfile(inputFile):
        
        process_file_module1(inputFile)
        
        outputFile = 'result.txt'
        
        print("Reading process is OK")
        
        print("Writing process is OK")
        
        print("The final result is located at '" +str(os.getcwd()) + "\\" + outputFile)
    else:
        print(inputFile + " file does not exist. Please provide a valid file name")
        sys.exit()
main()

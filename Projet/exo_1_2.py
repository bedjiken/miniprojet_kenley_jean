import os, os.path, sys, time
os.chdir('./File')

def process_file_module1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        return words

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file")

def process_file_module2(words):

    if words:
        try:
            counts = {} # Counts each words in the file in store them in a dic as key->value

            for w in words:
                counts[w] = counts.get(w, 0) + 1
            print("")
            print("The total number of words in the book is " + str(len(words)))
            print("")
            pairs = list(counts.items())
            items = [[x, y] for (y, x) in pairs]
            items.sort()

            for freq, word in items[0: len(items)]:
                print(word + "\t" + str(freq))

            print("")
            different_words = set(words)
            print("The number of differents words used in the book is " + str(len(different_words)))

        except IOError:
            print("Error")

def compare_book_by_authors(book1, book2):

    if os.path.isfile(book1) and os.path.isfile(book2):

        different_words_book1 = set(process_file_module1(book1))
        different_words_book2 = set(process_file_module1(book2))

        return len(different_words_book1), len(different_words_book2)

    else:
        print("File(s) don't exist")
        sys.exit()
    
def main():
    
    fileName = input("Enter a file name to process: ")

    words = process_file_module1(fileName)
    process_file_module2(words)

    print("------------------------------------------------------")
    print("Enter books from different authors you wish to compare:")

    book1 = input("Enter file 1: ")
    book2 = input("Enter file 2: ")

    lst_book1 = book1.split('.')
    lst_book2 = book2.split('.')

    res1, res2 = compare_book_by_authors(book1, book2)

    print("")
    print("Different words used by both authors are " + str(res1) + " and " + str(res2) + " respectively.")

    if res1 == res2:
        print("Both authors used the same extensive vocabulary")
    elif res1 > res2:
        print("The authors of the " + lst_book1[0] + " book uses the most extensive vocabulary than the author of the " + lst_book2[0] + " book")
    else:
        print("The authors of the " + lst_book2[0] + " book uses the most extensive vocabulary than the author of the " + lst_book1[0] + " book")
    
main()
    

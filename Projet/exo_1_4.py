import os, os.path, sys
os.chdir('./File')

def process_file_module1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        counts = {} # Counts each words in the file in store them in a dic as key->value

        for w in words:
            counts[w] = counts.get(w, 0) + 1

        return counts
        
    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")

def compare_wordList_book(book, wordList):

    if book and wordList:
        s_book = book
        s_wordList = wordList
        new_set = set()

        for each_item1 in s_book:
                if each_item1 not in s_wordList:
                        new_set.add(each_item1)
        
        print("")
        print("The words in the book that are not in the word list are as follow:")               
        for word in new_set:
            print(word)
        print("Above are the words in the book that are not in the word list.")
    else:
        sys.exit()


def main():

    word_list = input("Enter the word list: ")
    book = input("Enter the book name: ")

    if os.path.isfile(word_list) and os.path.isfile(book):
        if word_list == book:
            print("Files name are the same, there is no comparision to take into account.")
        else:
            word = process_file_module1(word_list)
            book = process_file_module1(book)

            compare_wordList_book(book, word)
    else:
        print("File don't exist")
        sys.exit()
    
main()

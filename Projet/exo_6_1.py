import os, os.path, sys
os.chdir('./File')

def process_file_module1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        return set(words)

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")

def set_substraction(book, wordList):

    if book and wordList:

        result = book.difference(wordList)
        print("")
        print("Words in the book that are not in the word list using set substraction are:")
        for word in result:
            print(word)

    else:
        sys.exit()

def main():

    word_list = input("Enter the word list to compare: ")
    book = input("Enter the book name to compare: ")

    if os.path.isfile(word_list) and os.path.isfile(book):
        word = process_file_module1(word_list)
        book = process_file_module1(book)

        set_substraction(book, word)

    else:
        print("File(s) dont exist")
        sys.exit()
    
main()

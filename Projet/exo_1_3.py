import os, os.path, sys
os.chdir('./File')

def process_file_module1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        return words

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")

def process_file_module2(words):

    if words:
        try:
            counts = {} # Counts each words in the file in store them in a dic as key->value

            for w in words:
                counts[w] = counts.get(w, 0) + 1

            pairs = list(counts.items())

            return pairs

        except IOError:
            print("Error")

def process_file_module3(pairs):

    if pairs:
        most_used_words = pairs
        lst_most_used_words = []

        lst_most_used_words = [[x, y] for (y, x) in pairs]
        
        lst_most_used_words.sort(reverse = True)
        
        sum = 0        
        print("The 20 most frequently-used words in the book are:\n")
        for freq, word in lst_most_used_words[0: 21]:
            sum += freq
            print(word + "\t" + str(freq))

        print("")
        print("The total of the 20 most frequently-used words in the books is " + str(sum) + ".")
    else:
        sys.exit()

def main():

    fileName = input("Enter the file name to process: ")
    print("")

    words = process_file_module1(fileName)

    pairs = process_file_module2(words)

    process_file_module3(pairs)

main()
        
    
    

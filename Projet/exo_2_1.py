import os, os.path, sys
os.chdir('./File')

#Construct a histogram
def process_file_module2_1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        counts = {} # Counts each words in the file in store them in a dic as key->value

        for w in words:
            counts[w] = counts.get(w, 0) + 1
        
        return counts, len(words)

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")

def choose_from_hist(histogram, univers_length = None):

    if histogram:
        items = list(histogram.items())
        univers_length = univers_length

        print("")
        print("The random value from the histogram, chosen with probability in proportion to frequency is:")
        for i in range(len(histogram)):
            print(str(items[i][0]) + "\t\t" + str(items[i][1]) + "/" + str(univers_length))
            break

        print("")
        print_ = input("Do you wish to print all the words with their respective probability on the screen? Yes or No: ")

        if print_ == 'Yes':
            for i in range(len(histogram)):
                print(str(items[i][0]) + "\t\t" + str(items[i][1]) + "/" + str(univers_length))
        else:
            sys.exit()
            
    
def main():

    fileName = input("Enter the file name to process: ")

    if os.path.isfile(fileName):
        histogram, univers_length = process_file_module2_1(fileName)
        choose_from_hist(histogram, univers_length)
    else:
        print(fileName + " file does not exist. Please provide a valid file name")
        sys.exit()
main()

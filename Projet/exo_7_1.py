import os, os.path, sys, time, random
os.chdir('./File')

def process_file_module1(fileName):

    try:
        infile = open(fileName, 'r')
        data = infile.read()

        text = data.lower()

        for ch in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
            text = text.replace(ch, '')

        words = text.split()

        counts = {}

        for word in words:
            counts[word] = counts.get(word, 0) + 1

        return counts

    except IOError:
        print(fileName + " file does not exist. Please provide a valid file name")


#function for returning a random word
def random_word(histogram):
            
    keys = list(histogram.keys()) #1 get a list of the words in the book

    values = list(histogram.values()) #2 . Build a list that contains the cumulative sum of the word frequencies
    values.sort()
    sorted_values = values # I sort the list, why? Because to use binary searc on it it must be sorted first so i sort it
    
    # cummulative sum
    total  = 0
    cum_sum = [] # contains the cummulative sum in the list

    for v in values:
      total = total + v
      cum_sum.append(total)

    return keys, sorted_values, cum_sum # return three values

#binary search
def binarySearch(sequence, value):

    low, hi = 0, len(sequence) - 1

    while low <= hi:

        mid = (low + hi) // 2
        if sequence[mid] < value:
            low = mid + 1
        elif value < sequence[mid]:
            hi = mid - 1
        else:
            return mid
          
    return -low - 1

def corresponding_word(counts, random_value):

    pairs = list(counts.items())
    items = [[x, y] for (y, x) in pairs]
    items.sort()

    for i in range(0, len(items)):
            if items[i][0] == random_value:
                print("")
                print("The corresponding word in the word list is", items[i][1])
                break

def main():

    fileName = input("Enter the file name: ")

    if os.path.isfile(fileName):
    
        word = process_file_module1(fileName)

        lst_word, values, cum_sum = random_word(process_file_module1(fileName))

        print("")
        print("The list of the words in the book are: ")
        print(lst_word)

        print("")
        print("The list that contains the cumulative sum of the word frequencies is: ")
        print(cum_sum)

        print("")
        print("The total number of words in the list is:", cum_sum[len(cum_sum) - 1])
        print("")

        random_value = random.choice(values) # choose a random value from the cum_sum
        print("The random value chosen from the cumulative sum is", random_value)

        print("")
        print("The index where the random number would be inserted in the cumulative sum is", binarySearch(cum_sum, random_value))
        corresponding_word(process_file_module1(fileName), random_value)
    else:
        print("File doesn't exist")
        sys.exit()
    
main()
